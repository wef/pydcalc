/* imported by dcalc - supplied by curses, hpcalc, ibm etc */
#ifdef __cplusplus
extern "C" {
#endif

/* these need to match values in pydcalc: */
/* not needed: #define PRINBASE_INDEX 0 */
#define PRINT_INV_INDEX 1
#define PRINT_X_INDEX 3
#define PRINT_Y_INDEX 4
#define PRINT_Z_INDEX 5
#define PRINT_T_INDEX 6
#define PRINT_L_INDEX 7
#define DISPNUMS_INDEX 8
#define DISPREG_INDEX 9
#define DISPREGS_INDEX 10
#define MSG_INDEX 11
#define CLEAR_MSG_INDEX 12
#define POP_UP_REG_INDEX 13
#define POP_UP_HELP_INDEX 14
#define OS_RAW_MODE_INDEX 15
#define OS_INIT_INDEX 16
#define OS_TERM_INDEX 17
#define DIALOG_INDEX 18
#define PLACES_INDEX 19
#define STORE_INDEX 20
#define RECALL_INDEX 21
#define SAVEGUISETTINGS_INDEX 22
#define READGUISETTINGS_INDEX 23
/* HAS_ALGEBRAIC_MODE only */
#define ADD_X_INDEX 24
#define MAX_INDEX 24

    extern void print_string(char *buf);
    extern void prinbase(void);
    extern void print_inv(void);
    extern void print_deg(void);
    extern void print_x(char *buf);
    extern void print_y(char *buf);
    extern void print_z(char *buf);
    extern void print_t(char *buf);
    extern void print_l(char *buf);
#ifdef HAS_ALGEBRAIC_MODE
    extern void add_x(char *buf);
#endif
    extern void dispnums(void);
    extern void dispreg(int);
    extern void dispregs(void);
    extern void put_a_char(int i);
    extern int get_a_char(int *);
    extern void msg(char *buf);
    extern void clear_msg(void);
    extern void pop_up_reg(void);
    extern void pop_up_help(void);
    extern void os_raw_mode(int);
    extern void os_init(void);
    extern void os_term(void);
    extern int dialog(char *buf);
    extern int places(char *buf);
    extern int eval(char *, char *);
    extern int store(char *buf);
    extern int recall(char *buf);
    extern void saveGuiSettings(FILE *f);
    extern void readGuiSettings(char *b);
#ifdef __cplusplus
}
#endif
