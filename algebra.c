/* A Bison parser, made from algebra.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

# define	INUM	257
# define	NUM	258
# define	FNCT	259
# define	IFNCT	260
# define	OR	261
# define	AND	262
# define	XOR	263
# define	LEFT	264
# define	RIGHT	265
# define	yPx	266
# define	yCx	267
# define	DAYS	268
# define	TODAY	269
# define	RECTPOLAR	270
# define	POLARRECT	271
# define	MOD	272
# define	NEG	273
# define	POS	274
# define	NOT	275
# define	PCH	276

#line 3 "algebra.y"

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "dcalc.h"
	extern char *parserPointer;
	extern void msg(char*);

	struct funcName_s {
		char *fname;
		int (*func)();
	};
	double yy_tempf;

#ifdef DEBUG
#  define TRACE(x) x
#else
#  define TRACE(x)
#endif

	static int exec_x();
	static int exec_y();
	static int exec_z();
	static int exec_t();
	static int exec_u();
	static int exec_l();
	extern struct funcName_s *getsym(char *sym_name, int *len);
	extern int yyerror(char *);
	extern int yylex(void);
	extern int yyparse(void);

#line 36 "algebra.y"
#ifndef YYSTYPE
typedef union {
	long   ival;
	double val;  				 /* For returning numbers.                   */
	struct funcName_s  *fptr; /* For returning symbol-table pointers      */
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
#ifndef YYDEBUG
# define YYDEBUG 0
#endif



#define	YYFINAL		84
#define	YYFLAG		-32768
#define	YYNTBASE	33

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 276 ? yytranslate[x] : 36)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    28,     2,     2,     2,    26,     2,     2,
      31,    32,    20,    13,    30,    12,     2,    21,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    29,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    14,    15,    16,    17,
      18,    19,    22,    23,    24,    25,    27
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     2,     4,     6,     8,    13,    17,    21,    25,
      29,    34,    39,    44,    49,    52,    55,    59,    63,    67,
      71,    75,    77,    80,    84,    88,    92,    94,    96,   101,
     104,   108,   112,   116,   120,   124,   128,   132,   136,   140,
     144,   147,   150,   154
};
static const short yyrhs[] =
{
      34,     0,    35,     0,     4,     0,     5,     0,     5,    31,
      34,    32,     0,    34,    13,    34,     0,    34,    12,    34,
       0,    34,    20,    34,     0,    34,    21,    34,     0,    34,
      13,    34,    26,     0,    34,    12,    34,    26,     0,    34,
      20,    34,    26,     0,    34,    21,    34,    26,     0,    12,
      34,     0,    13,    34,     0,    34,    14,    34,     0,    34,
      15,    34,     0,    34,    16,    34,     0,    34,    18,    34,
       0,    34,    19,    34,     0,    17,     0,    34,    28,     0,
      31,    34,    32,     0,    34,    29,    34,     0,    34,    27,
      34,     0,     3,     0,     6,     0,     6,    31,    35,    32,
       0,    25,    35,     0,    35,     8,    35,     0,    35,    22,
      35,     0,    35,     7,    35,     0,    35,     9,    35,     0,
      35,    10,    35,     0,    35,    11,    35,     0,    35,    13,
      35,     0,    35,    12,    35,     0,    35,    20,    35,     0,
      35,    21,    35,     0,    12,    35,     0,    13,    35,     0,
      31,    35,    32,     0,    35,    29,    35,     0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,    69,    69,    71,    72,    73,    74,    75,    76,    78,
      80,    87,    94,   118,   125,   130,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   160,
     161,   166,   170,   171
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "INUM", "NUM", "FNCT", "IFNCT", "OR", "AND", 
  "XOR", "LEFT", "RIGHT", "'-'", "'+'", "yPx", "yCx", "DAYS", "TODAY", 
  "RECTPOLAR", "POLARRECT", "'*'", "'/'", "MOD", "NEG", "POS", "NOT", 
  "'%'", "PCH", "'!'", "'^'", "','", "'('", "')'", "dummy", "exp", "iexp", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    33,    33,    34,    34,    34,    34,    34,    34,    34,
      34,    34,    34,    34,    34,    34,    34,    34,    34,    34,
      34,    34,    34,    34,    34,    34,    35,    35,    35,    35,
      35,    35,    35,    35,    35,    35,    35,    35,    35,    35,
      35,    35,    35,    35
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     1,     1,     1,     1,     4,     3,     3,     3,     3,
       4,     4,     4,     4,     2,     2,     3,     3,     3,     3,
       3,     1,     2,     3,     3,     3,     1,     1,     4,     2,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     2,     3,     3
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
       0,    26,     3,     4,    27,     0,     0,    21,     0,     0,
       1,     2,     0,     0,    14,    40,    15,    41,     0,     0,
       0,    29,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    22,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    23,    42,     7,     6,    16,    17,    18,    19,
      20,     8,     9,    25,    24,    32,    30,    33,    34,    35,
      37,    36,    38,    39,    31,    43,     5,    28,    11,    10,
      12,    13,     0,     0,     0
};

static const short yydefgoto[] =
{
      82,    14,    15
};

static const short yypact[] =
{
      33,-32768,-32768,   -26,    10,    33,    33,-32768,    56,    33,
     219,   142,    78,    56,    28,    15,    28,    15,    56,    56,
      56,    15,   160,    90,    78,    78,    78,    78,    78,    78,
      78,    78,    78,    78,-32768,    78,    56,    56,    56,    56,
      56,    56,    56,    56,    56,    56,    56,    78,    78,    78,
     181,   116,-32768,-32768,   -13,    87,   -10,   -10,   -10,   -10,
     -10,   -25,    -6,    23,    31,   148,   194,   207,   229,   229,
      64,    64,    15,    15,    15,    15,-32768,-32768,-32768,-32768,
  -32768,-32768,    61,    63,-32768
};

static const short yypgoto[] =
{
  -32768,     0,    34
};


#define	YYLAST		258


static const short yytable[] =
{
      10,    80,    33,    34,    35,    12,    16,    31,    32,    22,
      31,    32,    50,    78,    33,    34,    35,    33,    34,    35,
      81,    33,    34,    35,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    11,    64,     1,     2,     3,     4,
      17,    13,    21,    23,    46,     5,     6,    51,    16,    22,
       7,    34,    35,    17,    23,    33,    34,    35,     8,     1,
      35,    83,     4,    84,     9,     0,     0,     0,    18,    19,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,     8,     2,     3,    43,    44,    45,    20,     0,     0,
      47,    48,     0,    46,     0,     7,     0,    36,    37,    38,
      39,    40,    41,    42,     0,     0,     0,    31,    32,    49,
      43,    44,    45,    79,    33,    34,    35,     0,     0,    46,
       0,     0,    53,    36,    37,    38,    39,    40,    41,    42,
       0,     0,     0,     0,     0,     0,    43,    44,    45,     0,
       0,     0,     0,     0,     0,    46,     0,     0,    77,    36,
      37,    38,    39,    40,    41,    42,    37,    38,    39,    40,
      41,    42,    43,    44,    45,     0,     0,     0,    43,    44,
      45,    46,    24,    25,    26,    27,    28,    46,    29,    30,
      31,    32,     0,     0,     0,     0,     0,    33,    34,    35,
       0,     0,    52,    24,    25,    26,    27,    28,     0,    29,
      30,    31,    32,    38,    39,    40,    41,    42,    33,    34,
      35,     0,     0,    76,    43,    44,    45,    39,    40,    41,
      42,     0,     0,    46,     0,     0,     0,    43,    44,    45,
       0,    24,    25,    26,    27,    28,    46,    29,    30,    31,
      32,    41,    42,     0,     0,     0,    33,    34,    35,    43,
      44,    45,     0,     0,     0,     0,     0,     0,    46
};

static const short yycheck[] =
{
       0,    26,    27,    28,    29,    31,     6,    20,    21,     9,
      20,    21,    12,    26,    27,    28,    29,    27,    28,    29,
      26,    27,    28,    29,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,     0,    35,     3,     4,     5,     6,
       6,    31,     8,     9,    29,    12,    13,    13,    48,    49,
      17,    28,    29,    19,    20,    27,    28,    29,    25,     3,
      29,     0,     6,     0,    31,    -1,    -1,    -1,    12,    13,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    25,     4,     5,    20,    21,    22,    31,    -1,    -1,
      12,    13,    -1,    29,    -1,    17,    -1,     7,     8,     9,
      10,    11,    12,    13,    -1,    -1,    -1,    20,    21,    31,
      20,    21,    22,    26,    27,    28,    29,    -1,    -1,    29,
      -1,    -1,    32,     7,     8,     9,    10,    11,    12,    13,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    29,    -1,    -1,    32,     7,
       8,     9,    10,    11,    12,    13,     8,     9,    10,    11,
      12,    13,    20,    21,    22,    -1,    -1,    -1,    20,    21,
      22,    29,    12,    13,    14,    15,    16,    29,    18,    19,
      20,    21,    -1,    -1,    -1,    -1,    -1,    27,    28,    29,
      -1,    -1,    32,    12,    13,    14,    15,    16,    -1,    18,
      19,    20,    21,     9,    10,    11,    12,    13,    27,    28,
      29,    -1,    -1,    32,    20,    21,    22,    10,    11,    12,
      13,    -1,    -1,    29,    -1,    -1,    -1,    20,    21,    22,
      -1,    12,    13,    14,    15,    16,    29,    18,    19,    20,
      21,    12,    13,    -1,    -1,    -1,    27,    28,    29,    20,
      21,    22,    -1,    -1,    -1,    -1,    -1,    -1,    29
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/share/bison/bison.simple"

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

#line 315 "/usr/share/bison/bison.simple"


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 2:
#line 69 "algebra.y"
{ yyval.val = (long) yyvsp[0].ival; }
    break;
case 3:
#line 71 "algebra.y"
{ TRACE(printf("yyparse: num\n")); yyval.val = yyvsp[0].val; pushf(yyvsp[0].val);}
    break;
case 4:
#line 72 "algebra.y"
{ TRACE(printf("yyparse: func\n")); yyval.val = (*(yyvsp[0].fptr->func))(); }
    break;
case 5:
#line 73 "algebra.y"
{ TRACE(printf("yyparse: func\n")); yyval.val = (*(yyvsp[-3].fptr->func))(); }
    break;
case 6:
#line 74 "algebra.y"
{ TRACE(printf("yyparse: +\n")); exec_plus(); yyval.val = xfReg; }
    break;
case 7:
#line 75 "algebra.y"
{ TRACE(printf("yyparse: -\n")); exec_minus(); yyval.val = xfReg; }
    break;
case 8:
#line 76 "algebra.y"
{ TRACE(printf("yyparse: *\n")); exec_mult(); yyval.val = xfReg; }
    break;
case 9:
#line 78 "algebra.y"
{ TRACE(printf("yyparse: /\n")); exec_divide(); yyval.val = xfReg; }
    break;
case 10:
#line 80 "algebra.y"
{ 
	   TRACE(printf("yyparse: +%%\n")); 
	   pushf(1.0 + popf() / 100.0);
	   exec_mult();
	   yyval.val = xfReg; 
   }
    break;
case 11:
#line 87 "algebra.y"
{ 
	   TRACE(printf("yyparse: -%%\n")); 
	   pushf(1.0 - popf() / 100.0);
	   exec_mult();
	   yyval.val = xfReg; 
   }
    break;
case 12:
#line 94 "algebra.y"
{ 
	   TRACE(printf("yyparse: *%%\n")); 
	   pushf(popf() / 100.0);
	   exec_mult();
	   yyval.val = xfReg; 
   }
    break;
case 13:
#line 118 "algebra.y"
{ 
	   TRACE(printf("yyparse: /%%\n")); 
	   pushf(popf() / 100.0);
	   exec_divide();
	   yyval.val = xfReg; 
   }
    break;
case 14:
#line 125 "algebra.y"
{ 
	   TRACE(printf("yyparse: unary -\n")); 
	   exec_chs();
	   yyval.val = xfReg;
   }
    break;
case 15:
#line 130 "algebra.y"
{ 
	   TRACE(printf("yyparse: unary +\n")); 
	   yyval.val = xfReg;
   }
    break;
case 16:
#line 134 "algebra.y"
{ TRACE(printf("yyparse: PERM\n")); exec_perm(); yyval.val = xfReg; }
    break;
case 17:
#line 135 "algebra.y"
{ TRACE(printf("yyparse: COMB\n")); exec_comb(); yyval.val = xfReg; }
    break;
case 18:
#line 136 "algebra.y"
{ TRACE(printf("yyparse: DAYS\n")); exec_dys(); yyval.val = xfReg; }
    break;
case 19:
#line 137 "algebra.y"
{ TRACE(printf("yyparse: RECTPOLAR\n")); exec_rtop(); yyval.val = xfReg; }
    break;
case 20:
#line 138 "algebra.y"
{ TRACE(printf("yyparse: POLARRECT\n")); exec_ptor(); yyval.val = xfReg; }
    break;
case 21:
#line 139 "algebra.y"
{ TRACE(printf("yyparse: TODAY\n")); exec_tdy(); yyval.val = xfReg; }
    break;
case 22:
#line 140 "algebra.y"
{ TRACE(printf("yyparse: factorial\n")); exec_fact(); yyval.val = xfReg; }
    break;
case 23:
#line 141 "algebra.y"
{ TRACE(printf("yyparse: brackets\n")); yyval.val = yyvsp[-1].val; }
    break;
case 24:
#line 142 "algebra.y"
{ TRACE(printf("yyparse: ^\n")); exec_ytox(); yyval.val = xfReg; }
    break;
case 25:
#line 143 "algebra.y"
{ TRACE(printf("yyparse: PERCENTCH\n")); exec_percentch(); yyval.val = xfReg; }
    break;
case 26:
#line 146 "algebra.y"
{ TRACE(printf("yyparse: inum\n")); yyval.ival = yyvsp[0].ival; push(yyvsp[0].ival); }
    break;
case 27:
#line 147 "algebra.y"
{ TRACE(printf("yyparse: func\n")); yyval.ival = (*(yyvsp[0].fptr->func))(); }
    break;
case 28:
#line 148 "algebra.y"
{ TRACE(printf("yyparse: func\n")); yyval.ival = (*(yyvsp[-3].fptr->func))(); }
    break;
case 29:
#line 149 "algebra.y"
{ TRACE(printf("yyparse: NOT\n")); exec_not(); yyval.ival = xiReg; }
    break;
case 30:
#line 150 "algebra.y"
{ TRACE(printf("yyparse: AND\n")); exec_and(); yyval.ival = xiReg; }
    break;
case 31:
#line 151 "algebra.y"
{ TRACE(printf("yyparse: AND\n")); exec_modulus(); yyval.ival = xiReg; }
    break;
case 32:
#line 152 "algebra.y"
{ TRACE(printf("yyparse: OR\n")); exec_or(); yyval.ival = xiReg; }
    break;
case 33:
#line 153 "algebra.y"
{ TRACE(printf("yyparse: XOR\n")); exec_xor(); yyval.ival = xiReg; }
    break;
case 34:
#line 154 "algebra.y"
{ TRACE(printf("yyparse: LEFT\n")); exec_shiftyl(); yyval.ival = xiReg; }
    break;
case 35:
#line 155 "algebra.y"
{ TRACE(printf("yyparse: RIGHT\n")); exec_shiftyr(); yyval.ival = xiReg; }
    break;
case 36:
#line 156 "algebra.y"
{ TRACE(printf("yyparse: +\n")); exec_plus(); yyval.ival = xiReg; }
    break;
case 37:
#line 157 "algebra.y"
{ TRACE(printf("yyparse: -\n")); exec_minus(); yyval.ival = xiReg; }
    break;
case 38:
#line 158 "algebra.y"
{ TRACE(printf("yyparse: *\n")); exec_mult(); yyval.ival = xiReg; }
    break;
case 39:
#line 160 "algebra.y"
{ TRACE(printf("yyparse: /\n")); exec_divide(); yyval.ival = xiReg; }
    break;
case 40:
#line 161 "algebra.y"
{ 
		TRACE(printf("yyparse: unary -\n")); 
		exec_chs();
		yyval.ival = xiReg;
	}
    break;
case 41:
#line 166 "algebra.y"
{ 
 	   TRACE(printf("yyparse: unary +\n")); 
	   yyval.ival = xiReg;
    }
    break;
case 42:
#line 170 "algebra.y"
{ TRACE(printf("yyparse: brackets\n")); yyval.ival = yyvsp[-1].ival; }
    break;
case 43:
#line 171 "algebra.y"
{ TRACE(printf("yyparse: ^\n")); exec_ytox(); yyval.ival = xiReg; }
    break;
}

#line 705 "/usr/share/bison/bison.simple"


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
#line 175 "algebra.y"


static int exec_x() { (mode == PROG)? push(xSave): pushf(xfSave); return 0; }
static int exec_y() { (mode == PROG)? push(ySave): pushf(yfSave); return 0; }
static int exec_z() { (mode == PROG)? push(zSave): pushf(zfSave); return 0; }
static int exec_t() { (mode == PROG)? push(tSave): pushf(tfSave); return 0; }
static int exec_u() { (mode == PROG)? push(uSave): pushf(ufSave); return 0; }
static int exec_l() { (mode == PROG)? push(lSave): pushf(lfSave); return 0; }

struct funcName_s fncts[] ={
	{ "%",			exec_percent },
	{ "<<",			exec_shiftyl },
	{ ">>",			exec_shiftyr },
	{ "acos",		exec_acos },
	{ "acosh",		exec_acosh },
	{ "alog10",		exec_10tox },
	{ "aloge",		exec_etox },
	{ "and",		exec_and },
	{ "ans",    	exec_y },
	{ "asin",		exec_asin },
	{ "asinh",		exec_asinh },
	{ "atan",		exec_atan },
	{ "atanh",		exec_atanh },
	{ "cos",		exec_cos },
	{ "cosh",		exec_cosh },
	{ "croot",		exec_croot },
	{ "cube",		exec_cube },
	{ "ch%",		exec_percentch },
	{ "dtor",		exec_dtor },
	{ "dys",		exec_dys },
	{ "e",			exec_e }, /* Note 'e' is a valid hex number */
	{ "etox",		exec_etox },
	{ "fact",		exec_fact },
	{ "fclr",		exec_fclr },
	{ "frc",		exec_frc },
	{ "fval",		exec_fval },
	{ "fx",			exec_calcy },
	{ "hms",		exec_hms },
	{ "int",		exec_int },
	{ "intst",		exec_intst },
	{ "l",	    	exec_l },
	{ "lastx",		exec_lastx },
	{ "log10",		exec_log10 },
	{ "loge",		exec_loge },
	{ "lr",			exec_lr },
	{ "mean",		exec_mean },
	{ "means",		exec_means },
	{ "meany",		exec_meany },
	{ "mod",		exec_modulus },
	{ "nfin",		exec_nfin },
	{ "not",		exec_not },
	{ "nstat",		exec_nstat },
	{ "or",			exec_or },
	{ "pf",			exec_primef },
	{ "pi",			exec_pi },
	{ "plus",		exec_plus },
	{ "pmt",		exec_pmt },
	{ "ptor",		exec_ptor },
	{ "pval",		exec_pval },
	{ "rcl",		exec_rcl },
	{ "rolldown",	exec_rolldown },
	{ "rtod",		exec_rtod },
	{ "rtop",		exec_rtop },
	{ "s_dev",		exec_s_dev },
	{ "sin",		exec_sin },
	{ "sinh",		exec_sinh },
	{ "sqrt",		exec_root },
	{ "sum",		exec_sum },
	{ "sum0",		exec_sum0 },
	{ "sumr",		exec_sumr },
	{ "t",	    	exec_t },
	{ "tan",		exec_tan },
	{ "tanh",		exec_tanh },
	{ "tdy",		exec_tdy },
	{ "u",	    	exec_u },
	{ "x",	    	exec_x },
	{ "xnl",		exec_xnl },
	{ "xnt",		exec_xnt },
	{ "xny",		exec_xny },
	{ "xnz",		exec_xnz },
	{ "xor",		exec_xor },
	{ "y",	    	exec_y },
	{ "ycx",		exec_comb },
	{ "ypx",		exec_perm },
	{ "z",	    	exec_z },
	{ 0,			0 }
 };

struct funcName_s *
getsym (char *sym_name, int *len) {
	struct funcName_s *ptr;
	int i;
	static char *symbuf = 0;
	char c;
	static int length = 0;

	/* Initially make the buffer long enough
	   for a 40-character symbol name.  */
	if (length == 0) {
		length = 40;
		symbuf = (char *)malloc (length + 1);
	}

	i = 0;
	c = tolower(sym_name[0]);
	do {
		/* If buffer is full, make it bigger.        */
		if (i == length) {
			length *= 2;
			symbuf = (char *)realloc (symbuf, length + 1);
		}
		/* Add this character to the buffer.         */
		symbuf[i++] = c;
		/* Get another character.                    */
		c = tolower(*++sym_name);
	} while (c != 0 && (isalnum (c) || 
						(i == 1 && c == symbuf[0])));

	symbuf[i] = '\0';
	TRACE(printf("getsym: searching for <%s>\n", symbuf);)
	*len = strlen(symbuf);
	for (ptr = fncts; 
		 ptr->fname != (char *) 0;
		 ptr++)
		if (strcmp(ptr->fname, symbuf) == 0)
			return ptr;
	return 0;
}

int
yylex (void) {
	char *endptr;
    int c;

    /* Ignore whitespace, get first nonwhite character.  */
    while ((c = *parserPointer) != 0 && (c == ' ' || 
										 c == '\t'))
		parserPointer++;

    if (c == 0)
		return 0;

	/* These seem to be alpha to isalpha() and to be affected by
	 * tolower(), at least on the bookman. Unix seems ok tho'!: */
	if (c == '�' || c == '�') {
		TRACE(printf("yylex: TOKEN <%c>\n", c));
		parserPointer++;
		return c=='�'? '*': '/';
	}

	c = tolower(*parserPointer);

	if (mode == PROG) {
		switch (intmode) {
		case BIN: 
			if (c == '0' || c == '1') {
				yylval.ival = strtol(parserPointer, &endptr, 2);
				parserPointer = endptr;
				TRACE(printf("yylex: INUM %ld\n", yylval.ival));
				return INUM;
			}
			break;
		case OCT:
			if (c >= '0' && c <= '7') {
				yylval.ival = strtol(parserPointer, &endptr, 8);
				parserPointer = endptr;
				TRACE(printf("yylex: INUM 0%lo\n", yylval.ival));
				return INUM;
			}
			break;
		case DEC:
			if (c >= '0' && c <= '9') {
				yylval.ival = strtol(parserPointer, &endptr, 10);
				parserPointer = endptr;
				TRACE(printf("yylex: INUM %ld\n", yylval.ival));
				return INUM;
			}
			break;
		case HEX: 
			if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f')) {
				int len;
				struct funcName_s *s;

				/* extra care here as functions starting with a-f
				 * could get here */
				if (isalpha(c) && (s = getsym(parserPointer, &len)) != NULL)
					if (s->func != exec_e) /* E by itself */
						break;

				yylval.ival = strtol(parserPointer, &endptr, 16);
				parserPointer = endptr;
				TRACE(printf("yylex: INUM 0x%lx\n", yylval.ival));
				return INUM;
			}
			break;
		case IP: /* look for 4 decimal numbers from 0-255 separated by . */
			do {
				int i, num[4], count = 0;
				for (i = 0; i < 4; i++) {
					TRACE(printf("parserPointer = <%s>\n", parserPointer);)
					num[i] = 0;
					if (!*parserPointer ||
						(*parserPointer < '0' || *parserPointer > '9'))
						break;
					count++;
					num[i] = strtol(parserPointer, &endptr, 10);
					TRACE(printf("num[%d]=%d\n", i, num[i]);)
					parserPointer = endptr;
					if (*parserPointer == '.')
						parserPointer++;
				}
				TRACE(printf("count=%d\n", count);)
				yylval.ival = 0;
				for (i = 0; count--; i++) {
					yylval.ival = (yylval.ival << 8) | num[i];
					TRACE(printf("%d: yylval.ival = %ld\n", i, yylval.ival);)
				}
				TRACE(printf("final yylval.ival = %ld\n", yylval.ival);)
			} while (0);
			return INUM;
			break; 
		}
	} else {
		if (c == '.' || isdigit (c)) {
			yylval.val = strtod(parserPointer, &endptr); /* check for HUGE_NUM & errno? */
			TRACE(printf("yylex: NUM %g\n", yylval.val));
			parserPointer = endptr;
			return NUM;
		}
	}

	if (c == '%' && tolower(*(parserPointer+1)) == 'c' && tolower(*(parserPointer+2)) == 'h') {
		parserPointer += 3;
		return PCH;
	}

	if (c == '<') {
		if (*(parserPointer+1) == '<') {
			parserPointer += 2;
			return LEFT;
		}
		parserPointer++;
		return LEFT;
	}

	if (c == '>') {
		if (*(parserPointer+1) == '>') {
			parserPointer += 2;
			return RIGHT;
		}
		parserPointer++;
		return RIGHT;
	}

    /* Char starts an identifier => read the name.       */
    if (isalpha (c)) {
		struct funcName_s *s;
		int len;

		s = getsym(parserPointer, &len);

		parserPointer += len;
		if (s == 0) {
			TRACE(printf("yylex: not found\n"));
			return 0;
		}

		if (s->func == exec_and) {
			TRACE(printf("yylex: AND\n"));
			return AND;
		}
		if (s->func == exec_or) {
			TRACE(printf("yylex: OR\n"));
			return OR;
		}
		if (s->func == exec_xor) {
			TRACE(printf("yylex: XOR \n"));
			return XOR;
		}
		if (s->func == exec_not) {
			TRACE(printf("yylex: NOT \n"));
			return NOT;
		}
		if (s->func == exec_shiftyl) {
			TRACE(printf("yylex: SHIFTYL \n"));
			return LEFT;
		}
		if (s->func == exec_shiftyr) {
			TRACE(printf("yylex: SHIFTYR \n"));
			return RIGHT;
		}
		if (s->func == exec_modulus) {
			TRACE(printf("yylex: MOD \n"));
			return MOD;
		}
		if (s->func == exec_comb) {
			TRACE(printf("yylex: COMB \n"));
			return yCx;
		}
		if (s->func == exec_perm) {
			TRACE(printf("yylex: PERM \n"));
			return yPx;
		}
		if (s->func == exec_dys) {
			TRACE(printf("yylex: DAYS \n"));
			return DAYS;
		}
		if (s->func == exec_tdy) {
			TRACE(printf("yylex: TODAY \n"));
			return TODAY;
		}
		if (s->func == exec_rtop) {
			TRACE(printf("yylex: RECTPOLAR \n"));
			return RECTPOLAR;
		}
		if (s->func == exec_ptor) {
			TRACE(printf("yylex: POLARRECT \n"));
			return POLARRECT;
		}

		yylval.fptr = s;
		if (mode == PROG) {
			TRACE(printf("yylex: IFNCT %s\n", s->fname));
			return IFNCT;
		} else {
			TRACE(printf("yylex: FNCT %s\n", s->fname));
			return FNCT;
		}
    }

    /* Any other character is a token by itself.        */
	TRACE(printf("yylex: TOKEN <%c>\n", c));
	parserPointer++;
    return c;
}

int
yyerror (char *s) { /* Called by yyparse on error */
	msg(s);
	lift_needed = 1;
	return 0;
}
