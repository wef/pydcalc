dcalc_SOURCES = \
	dcalc.c \
	dcalc.h \
	interface.c \
	interface.h

dcalc_OBJECTS = dcalc.o interface.o
TARGET=libdcalc.so

# CFLAGS = -DHAS_ALGEBRAIC_MODE -fPIC
CFLAGS = -fPIC -g
LDFLAGS = -lm -shared

$(TARGET): $(dcalc_OBJECTS)
	gcc $(LDFLAGS) -o $(TARGET) $(dcalc_OBJECTS)

clean:
	rm *.o *.so
