/* imported by dcalc - supplied by curses, hpcalc, ibm etc */

#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <float.h> /* for DBL_MAX */

extern int errno;
int is_resident;
int raw_mode; /* FIXME: needed? */

#include "interface.h"

/* These not needed for GTK version: */
void prinbase() {};
void print_string(char *buf) {}
void print_deg(){}
void pop_up_help(void) {}
void os_raw_mode(int i) { raw_mode = 0 /* i */; }
void os_init(void) {}
void os_term(void) {}
int get_a_char(int *c) {return 0;}

typedef int (*p_func_t) (char *);
p_func_t func_array[MAX_INDEX + 1];

int do_func(int index, char *buf) {
    printf("do_func(%d,'%s')\n", index, buf);
    p_func_t func;
    func = func_array[index];
    if (!func) {
        printf("do_func(%d): no func\n", index);
        return 1;
    } else
        return((*func)(buf));
}

void print_inv(void) {
    printf("print_inv\n");
}

void print_x(char *buf) {
    printf("print_x('%s')\n", buf);
    do_func(PRINT_X_INDEX, buf);
}

void print_y(char *buf) {
    printf("print_y('%s')\n", buf);
    do_func(PRINT_Y_INDEX, buf);
}

void print_z(char *buf) {
    printf("print_z('%s')\n", buf);
    do_func(PRINT_Z_INDEX, buf);
}

void print_t(char *buf) {
    printf("print_t('%s')\n", buf);
    do_func(PRINT_T_INDEX, buf);
}

void print_l(char *buf) {
    printf("print_l('%s')\n", buf);
    do_func(PRINT_L_INDEX, buf);
}

#ifdef HAS_ALGEBRAIC_MODE
void add_x(char *buf) {
    printf("add_x\n");
}
#endif

void dispreg(int) {
    printf("dispreg\n");
}

void put_a_char(int i) {
    printf("put_a_char (beep)\n");
}

void msg(char *buf) {
    do_func(MSG_INDEX, buf);
}

void clear_msg(void) {
    printf("clear_msg\n");
    msg("");
}

void pop_up_reg(void) {
    printf("pop_up_reg\n");
}

int dialog(char *buf) {
}

int places(char *buf) {
}

int store(char *buf) {
}

int recall(char *buf) {
}

void saveGuiSettings(FILE *f) {
    printf("saveGuiSettings\n");
}

void readGuiSettings(char *b) {
    printf("readGuiSettings\n");
}

void register_if(int index, p_func_t python_function) {
    printf("register_if %d\n", index);
    if (index < 0 || index > MAX_INDEX) {
        fprintf(stderr, "register_if: index %s out of range\n", index);
    } else {
        func_array[index] = python_function;
    }
}

#ifdef __cplusplus
}
#endif
